import h5py
from pylab import *
from scipy.stats import linregress

#---------------------------------------------------------------------------
def plot_results(datafile, suptitle=None):
    with h5py.File(datafile) as f:
        print(f['cmdline'][()])

        npx = f['npx'][()]
        npy = f['npy'][()]

        n_1d = f['n_1d'][()]
        n_2d = f['n_2d'][()]
        t_1d = f['t_1d'][()]
        t_2d = f['t_2d'][()]
        v_1d = f['v_1d'][()]
        v_2d = f['v_2d'][()]
        
        print('mean(v_x) =', mean(v_1d))
        
    x = arange(npx)
    y = arange(npy)
    T0 = 0.1
    
    rn = linregress(x[1:-1], n_1d[1:-1]/npy)
    rt = linregress(x[1:-1], t_1d[1:-1]/T0)

    gs = GridSpec(4,3)

    figure(figsize=(13,15))
    subplot(gs[0,0])
    plot(x, n_1d/npy, label=r'$\nu(x)$');
    plot(x,rn.intercept + rn.slope * x, ':', label=f'${rn.intercept:.3f}+({rn.slope:.3g})x$')
    xlabel('$x/2\\pi$')
    legend()
    grid(linestyle=':')
    title('1D density')

    subplot(gs[0,1])
    plot(x, t_1d / T0, label='$T_e(x) / \\bar T$');
    plot(x, 0.9 + 0.2 * x / npx, ':', label=f'$0.9 + 0.2x/L$')
    xlabel('$x/2\\pi$')
    legend()
    grid(linestyle=':')
    title('1D temperature')
    
    subplot(gs[0,2])
    plot(x, v_1d, label='$V_x(x)$');
    plot(x, ones_like(x) * mean(v_1d), ':', label='$\\bar V_x$')
    xlabel('$y/2\\pi$')
    legend()
    grid(linestyle=':')
    title('1D $V_x$')
    
    subplot(gs[1,:])
    imshow(n_2d, origin='lower')
    xlabel('$x/2\\pi$')
    ylabel('$y/2\\pi$')
    colorbar(orientation='horizontal', shrink=0.5, pad=0.25)
    title('2D density')

    subplot(gs[2,:])
    imshow(t_2d, origin='lower')
    xlabel('$x/2\\pi$')
    ylabel('$y/2\\pi$')
    colorbar(orientation='horizontal', shrink=0.5, pad=0.25)
    title('2D temperature')
    
    subplot(gs[3,:])
    imshow(v_2d, origin='lower')
    xlabel('$x/2\\pi$')
    ylabel('$y/2\\pi$')
    colorbar(orientation='horizontal', shrink=0.5, pad=0.25)
    title('2D $V_x$')
    
    tight_layout()

    if suptitle:
        gcf().suptitle(suptitle)
        gcf().subplots_adjust(top=0.93)

#---------------------------------------------------------------------------
def density_slope(datafile):
    with h5py.File(datafile) as f:
        npx = f['npx'][()]
        npy = f['npy'][()]
        n1d = f['n_1d'][()]
        
    x = arange(npx)
    r = linregress(x[1:-1], n1d[1:-1]/npy)
    return r.slope
