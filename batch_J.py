#!/usr/bin/env python3
import sys
import argparse
import te2d
import h5py
from numpy import *
from get_J import get_J

if __name__ == '__main__':
    parser = argparse.ArgumentParser(sys.argv[0])
    parser.add_argument('--gpu', dest='gpu', default='')
    parser.add_argument('--npx', dest='npx', default=108,    type=int)
    parser.add_argument('--npy', dest='npy', default=24,     type=int)
    parser.add_argument('--nt',  dest='nt',  default=5000,   type=int)
    parser.add_argument('--ns',  dest='ns',  default=5000,   type=int)
    parser.add_argument('--nr',  dest='nr',  default=100,    type=int)
    parser.add_argument('--I',   dest='I',   default=0.5,    type=float)
    parser.add_argument('--Kc',  dest='Kc',  default=0.0462, type=float)
    parser.add_argument('--eta', dest='eta', default=0.02,   type=float)
    parser.add_argument('--T0',  dest='T0',  default=0.1,    type=float)
    args = parser.parse_args(sys.argv[1:])

    te2d.context(args.gpu)

    K = linspace(0, 4, 5)
    J = zeros_like(K)

    for i,k in enumerate(K):
        J[i] = get_J(args.npx, args.npy, args.I,
                args.Kc * k, args.Kc * k, 0.0, args.eta, args.T0,
                0.2 * args.T0, args.nt, args.ns, args.nr)

        print(f'K={k}, J={J[i]}')

    with h5py.File('J.h5', 'w') as f:
        f['K'] = K
        f['J'] = J


