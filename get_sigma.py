#!/usr/bin/env python3
import sys
from tqdm import tqdm
import te2d
from scipy.stats import linregress
from numpy import *
from numpy.random import uniform

def get_sigma(npx, npy, I, Kx, Ky, Edc, eta, T0, dT, nt, ns):
    period = 2 * pi
    L = period * npx
    H = period * npy
    C = period * 3

    density = 0.5 * (5**0.5 - 1)
    n = int(npx * npy * density)

    c = uniform((0,0), (L,H), (n,2))
    p = zeros_like(c)

    sys = te2d.thermoelectric(n, L, H, C, I, Kx, Ky, Edc, eta, T0, dT)

    dt = 0.02
    nt_relax = nt // 5

    print(f"""
--------------------------------
npx  = {npx}
npy  = {npy}
I    = {I}
Kx   = {Kx}
Ky   = {Ky}
Edc  = {Edc}
eta  = {eta}
T0   = {T0}
dT   = {dT}
--------------------------------
L    = {L}
H    = {H}
n    = {n}
maxt = {nt * ns * dt}
--------------------------------

""")

    nu = zeros(npx)

    spx = 0

    for i in tqdm(range(nt)):
        sys.advance(c, p, ns, dt)

        if i >= nt_relax:
            spx += sum(p[:,0])

    spx = spx / (n * (nt - nt_relax))
    return spx * sqrt(density) / period / Edc
