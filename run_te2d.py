#!/usr/bin/env python3
import sys
import argparse
import te2d
import h5py
from numpy import *
from numpy.random import uniform
from numpy.linalg import norm
from tqdm import tqdm

# --- Command line parameters -----------------------------------------------
parser = argparse.ArgumentParser(sys.argv[0])
parser.add_argument('--out', dest='out', default='te2d')
parser.add_argument('--gpu', dest='gpu', default='')
parser.add_argument('--npx', dest='npx', default=108,    type=int)
parser.add_argument('--npy', dest='npy', default=24,     type=int)
parser.add_argument('--C',   dest='C',   default=6,      type=int)
parser.add_argument('--nt',  dest='nt',  default=5000,   type=int)
parser.add_argument('--ns',  dest='ns',  default=1000,   type=int)
parser.add_argument('--nr',  dest='nr',  default=100,    type=int)
parser.add_argument('--a_s', dest='a_s', default=0.7,    type=float)
parser.add_argument('--I',   dest='I',   default=0.5,    type=float)
parser.add_argument('--Kc',  dest='Kc',  default=0.0462, type=float)
parser.add_argument('--eta', dest='eta', default=0.02,   type=float)
parser.add_argument('--T0',  dest='T0',  default=0.1,    type=float)
parser.add_argument('--periodic', dest='periodic', action='store_true', default=False)
parser.add_argument('--Edc', dest='Edc', default=[4e-4], type=float, nargs='+')
parser.add_argument('--dT',  dest='dT',  default=[0.2],  type=float, nargs='+')
parser.add_argument('--K',   dest='K',   default=[1.52], type=float, nargs='+')
args = parser.parse_args(sys.argv[1:])

# --- Initialize GPU context ------------------------------------------------
te2d.context(args.gpu)

# ---------------------------------------------------------------------------
period = 2 * pi
L = period * args.npx
H = period * args.npy
C = period * args.C

density = 0.5 * (5**0.5 - 1)
n = int(args.npx * args.npy * density)

len_e = len(args.Edc)
len_t = len(args.dT)
len_k = len(args.K)

m = max(len_e, len_t, len_k)

Edc = zeros(m)
dT  = zeros(m)
K   = zeros((m, 2))
J   = zeros(m)
j   = zeros(m)

for i in range(m):
    Edc[i] = args.Edc[i % len_e]
    dT[i]  = args.T0 * args.dT[i % len_t]
    K[i,:] = args.Kc * args.K[i % len_k]

print('Edc =', Edc)
print('dT  =', dT)
print('K   =', K[:,0])

c = uniform((0,0), (L,H), (m,n,2))
p = zeros_like(c)

ode = te2d.thermoelectric(n, m, args.periodic, args.a_s, L, H, C,
        args.I, args.eta, args.T0, Edc, dT, K)

dt = 0.02
nt_relax = args.nt // 2
nsamples = 0

# --- Initialize averages ---------------------------------------------------
n_1d = zeros((m,args.npx))
n_2d = zeros((m,args.npy, args.npx))

t_1d = zeros((m,args.npx))
t_2d = zeros((m,args.npy, args.npx))

v_1d = zeros((m,args.npx))
v_2d = zeros((m, args.npy, args.npx))

vmean = zeros((m,2))

# --- Integrate over time ---------------------------------------------------
for i in tqdm(range(args.nt), ncols=64):
    ode.advance(c, p, j, dt, args.ns, args.nr)

    if i >= nt_relax:
        nsamples += 1
        J += j

        v = 0.5 * norm(p, axis=2)**2

        for e in range(m):
            n1 = histogram(c[e,:,0], bins=args.npx, range=(0,L))[0]
            n2 = histogram2d(c[e,:,1], c[e,:,0], bins=(args.npy, args.npx),
                    range=((0,H),(0,L)))[0]

            t1 = histogram(c[e,:,0], bins=args.npx, range=(0,L), weights=v[e])[0]
            t2 = histogram2d(c[e,:,1], c[e,:,0], bins=(args.npy, args.npx),
                    range=((0,H),(0,L)), weights=v[e])[0]

            v1 = histogram(c[e,:,0], bins=args.npx, range=(0,L), weights=p[e,:,0])[0]
            v2 = histogram2d(c[e,:,1], c[e,:,0], bins=(args.npy, args.npx),
                    range=((0,H),(0,L)), weights=p[e,:,0])[0]

            I1 = (n1 > 0)
            I2 = (n2 > 0)

            t1[I1] = t1[I1] / n1[I1]
            t2[I2] = t2[I2] / n2[I2]

            v1[I1] = v1[I1] / n1[I1]
            v2[I2] = v2[I2] / n2[I2]

            n_1d[e] += n1
            n_2d[e] += n2

            t_1d[e] += t1
            t_2d[e] += t2

            v_1d[e] += v1
            v_2d[e] += v2

            vmean[e] += mean(p[e], axis=0)

J /= nsamples

n_1d /= nsamples
n_2d /= nsamples

t_1d /= nsamples
t_2d /= nsamples

v_1d /= nsamples
v_2d /= nsamples

vmean /= nsamples

# --- Save results ----------------------------------------------------------
with h5py.File('{}.h5'.format(args.out), 'w') as f:
    f['cmdline'] = ' '.join(sys.argv)
    f['npx'] = args.npx
    f['npy'] = args.npy
    f['L'] = L
    f['H'] = H
    f['n'] = n
    f['m'] = m
    f['C'] = C / period
    f['Edc'] = Edc
    f['dT'] = dT
    f['K'] = K
    f['density'] = density
    f['dt'] = dt
    f['tmax'] = args.nt * args.ns * dt
    f['nt'] = args.nt
    f['ns'] = args.ns
    f['nr'] = args.nr
    f['J'] = J
    f['n_1d'] = n_1d
    f['n_2d'] = n_2d
    f['t_1d'] = t_1d
    f['t_2d'] = t_2d
    f['v_1d'] = v_1d
    f['v_2d'] = v_2d
    f['vmean'] = vmean
