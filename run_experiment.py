#!/usr/bin/env python3
import sys
from tqdm import tqdm
import te2d
from scipy.stats import linregress
from numpy import *
from numpy.random import uniform

def run_experiment(npx, npy, I, Kx, Ky, Edc, eta, T0, dT, nt, ns, nr):
    period = 2 * pi
    L = period * npx
    H = period * npy
    C = period * 3

    n = int(npx * npy * 0.5 * (5**0.5 - 1))

    c = uniform((0,0), (L,H), (n,2))
    p = zeros_like(c)

    sys = te2d.thermoelectric(n, L, H, C, I, Kx, Ky, Edc, eta, T0, dT)

    dt = 0.02
    nt_relax = nt // 5

    print(f"""
--------------------------------
npx  = {npx}
npy  = {npy}
I    = {I}
Kx   = {Kx}
Ky   = {Ky}
Edc  = {Edc}
eta  = {eta}
T0   = {T0}
dT   = {dT}
--------------------------------
L    = {L}
H    = {H}
n    = {n}
maxt = {nt * ns * dt}
--------------------------------

""")

    nu = zeros(npx)

    for i in tqdm(range(nt)):
        sys.advance(c, p, dt, ns, nr)

        if i >= nt_relax:
            nu += histogram(c[:,0], bins=npx, range=(0,L))[0]

    nu = nu / (nt - nt_relax)
    slope = linregress(arange(1,npx-1)*period, nu[1:-1])[0]
    return slope
