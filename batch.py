#!/usr/bin/env python3
import sys
import argparse
import te2d
import h5py
from numpy import *
from run_experiment import run_experiment

if __name__ == '__main__':
    parser = argparse.ArgumentParser(sys.argv[0])
    parser.add_argument('--gpu', dest='gpu', default='')
    parser.add_argument('--npx', dest='npx', default=108,    type=int)
    parser.add_argument('--npy', dest='npy', default=24,     type=int)
    parser.add_argument('--nt',  dest='nt',  default=5000,   type=int)
    parser.add_argument('--ns',  dest='ns',  default=1000,   type=int)
    parser.add_argument('--nr',  dest='nr',  default=100,    type=int)
    parser.add_argument('--I',   dest='I',   default=0.5,    type=float)
    parser.add_argument('--Kc',  dest='Kc',  default=0.0462, type=float)
    parser.add_argument('--Kx',  dest='Kx',  default=1.52,   type=float)
    parser.add_argument('--Ky',  dest='Ky',  default=1.52,   type=float)
    parser.add_argument('--eta', dest='eta', default=0.02,   type=float)
    parser.add_argument('--T0',  dest='T0',  default=0.1,    type=float)
    args = parser.parse_args(sys.argv[1:])

    te2d.context(args.gpu)

    nruns = 5

    slope = empty(nruns)

    dT = linspace(0.1 * args.T0, 0.3 * args.T0, nruns)
    for i in range(nruns):
        slope[i] = run_experiment(args.npx, args.npy, args.I,
                args.Kc * args.Kx, args.Kc * args.Ky, 0.0, args.eta, args.T0,
                dT[i], args.nt, args.ns, args.nr)

        print(f'dT = {dT}, slope = {slope}')

    with h5py.File('dT.h5', 'w') as f:
        f['dT'] = dT
        f['slope'] = slope


    Edc = linspace(2e-5, 4e-4, nruns)
    for i in range(nruns):
        slope[i] = run_experiment(args.npx, args.npy, args.I,
                args.Kc * args.Kx, args.Kc * args.Ky, Edc[i], args.eta,
                args.T0, 0.0, args.nt, args.ns)

        print(f'Edc = {Edc}, slope = {slope}')

    with h5py.File('Edc.h5', 'w') as f:
        f['Edc'] = Edc
        f['slope'] = slope


