#include <iostream>
#include <vector>
#include <random>

#include <vexcl/vexcl.hpp>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include "te_system.hpp"

namespace py = pybind11;

//---------------------------------------------------------------------------
vex::Context& ctx(std::string name = "") {
    static vex::Context c(
            vex::Filter::Env        &&
            vex::Filter::Name(name) &&
            vex::Filter::Count(1)
            );
    return c;
}

//---------------------------------------------------------------------------
struct thermoelectric {
    const int n, m;
    state_type C;
    state_type P;
    te_system  ode_sys;
    std::vector<double> t;

    thermoelectric(int n, int m, bool periodic, double screening, double L, double H,
            double C, double I, double eta, double T0,
            py::array_t<double> Edc, py::array_t<double> dT, py::array_t<double> K)
        : n(n), m(m), C(ctx(), n * m), P(ctx(), n * m),
          ode_sys(ctx(), n, m, periodic, screening, L, H, C, I, eta, T0,
                  reinterpret_cast<const double *>(Edc.data()),
                  reinterpret_cast<const double *>(dT.data()),
                  reinterpret_cast<const cl_double2*>(K.data())),
          t(m)
    {
    }

    void advance(py::array_t<double> c, py::array_t<double> p, py::array_t<double> J, double dt, int steps, int sample_rate) {
        static std::mt19937 gen;
        static std::uniform_int_distribution<size_t> seed;

        cl_double2 *cptr = reinterpret_cast<cl_double2*>(c.mutable_data());
        cl_double2 *pptr = reinterpret_cast<cl_double2*>(p.mutable_data());

        vex::copy(cptr, cptr + n * m, C.begin());
        vex::copy(pptr, pptr + n * m, P.begin());

        Stepper stepper;

        int nsamples = 0;

        double *j = J.mutable_data();
        std::fill(j, j + m, 0.0);

        for(int i = 0; i < steps; ++i) {
            stepper.do_step(std::ref(ode_sys), std::make_pair(std::ref(C), std::ref(P)), 0.0, dt);
            ode_sys.fluctuate(C, P, dt, seed(gen));
            ode_sys.fix_boundaries(C, P);

            if (i % sample_rate == 0) {
                ode_sys.reorder(C, P);
                ode_sys.heat_flow(C, P, t.data());

                for(int k = 0; k < m; ++k)
                    j[k] += t[k];

                ++nsamples;
            }
        }

        vex::copy(C.begin(), C.end(), cptr);
        vex::copy(P.begin(), P.end(), pptr);

        for(int k = 0; k < m; ++k)
            j[k] /= nsamples;
    }
};

//---------------------------------------------------------------------------
PYBIND11_MODULE(te2d, m) {
    m.def("context", [](std::string name) {
            std::ostringstream s; s << ctx(name); py::print(s.str());
            }, py::arg("name") = std::string(""));

    py::class_<thermoelectric>(m, "thermoelectric")
        .def(py::init<int, int, bool, double,
                double, double, double, double, double, double,
                py::array_t<double>, py::array_t<double>, py::array_t<double>>(),
                py::arg("n"),
                py::arg("m"),
                py::arg("periodic"),
                py::arg("screening"),
                py::arg("L"),
                py::arg("H"),
                py::arg("C"),
                py::arg("I"),
                py::arg("eta"),
                py::arg("T0"),
                py::arg("Edc"),
                py::arg("dT"),
                py::arg("K")
            )
        .def("advance", &thermoelectric::advance,
                py::arg("x"),
                py::arg("p"),
                py::arg("J"),
                py::arg("dt"),
                py::arg("steps"),
                py::arg("sample_rate")
            )
        ;
}
