#!/usr/bin/env python3
import sys
import argparse
import te2d
import h5py
from numpy import *
from numpy.random import uniform
from lxml import etree
from tqdm import tqdm

# --- Command line parameters -----------------------------------------------
parser = argparse.ArgumentParser(sys.argv[0])
parser.add_argument('--out', dest='out', default='te2d')
parser.add_argument('--gpu', dest='gpu', default='')
parser.add_argument('--npx', dest='npx', default=108,    type=int)
parser.add_argument('--npy', dest='npy', default=24,     type=int)
parser.add_argument('--C',   dest='C',   default=6,      type=int)
parser.add_argument('--nt',  dest='nt',  default=5000,   type=int)
parser.add_argument('--ns',  dest='ns',  default=1000,   type=int)
parser.add_argument('--nr',  dest='nr',  default=100,    type=int)
parser.add_argument('--a_s', dest='a_s', default=0.7,    type=float)
parser.add_argument('--I',   dest='I',   default=0.5,    type=float)
parser.add_argument('--Kc',  dest='Kc',  default=0.0462, type=float)
parser.add_argument('--periodic', dest='periodic', action='store_true', default=False)
parser.add_argument('--eta', dest='eta', default=0.02,   type=float)
parser.add_argument('--T0',  dest='T0',  default=0.1,    type=float)
parser.add_argument('--Edc', dest='Edc', default=[4e-4], type=float, nargs='+')
parser.add_argument('--dT',  dest='dT',  default=[0.2],  type=float, nargs='+')
parser.add_argument('--K',   dest='K',   default=[1.52], type=float, nargs='+')
args = parser.parse_args(sys.argv[1:])

# --- Initialize GPU context ------------------------------------------------
te2d.context(args.gpu)

# ---------------------------------------------------------------------------
period = 2 * pi
L = period * args.npx
H = period * args.npy
C = period * args.C

density = 0.5 * (5**0.5 - 1)
n = int(args.npx * args.npy * density)

len_e = len(args.Edc)
len_t = len(args.dT)
len_k = len(args.K)

m = max(len_e, len_t, len_k)

Edc = zeros(m)
dT  = zeros(m)
K   = zeros((m, 2))
J   = zeros(m)

for i in range(m):
    Edc[i] = args.Edc[i % len_e]
    dT[i]  = args.T0 * args.dT[i % len_t]
    K[i,:] = args.Kc * args.K[i % len_k]

print('Edc =', Edc)
print('dT  =', dT)
print('K   =', K[:,0])

c = uniform((0,0), (L,H), (m, n, 2))
p = zeros_like(c)

ode = te2d.thermoelectric(n, m, args.periodic, args.a_s, L, H, C,
        args.I, args.eta, args.T0, Edc, dT, K)

dt = 0.02
nu = zeros((m, args.npy, args.npx))

xmf = etree.Element('Xdmf', nsmap={
    "xi" : "http://www.w3.org/2003/XInclude"
    })

domain = etree.SubElement(xmf, 'Domain')

coo = etree.SubElement(domain, 'Grid')
coo.attrib['Name'] = 'Coordinates'
coo.attrib['GridType'] = 'Collection'
coo.attrib['CollectionType'] = 'Temporal'

hist = etree.SubElement(domain, 'Grid')
hist.attrib['Name'] = 'Density'
hist.attrib['GridType'] = 'Collection'
hist.attrib['CollectionType'] = 'Temporal'

with h5py.File('{}.h5'.format(args.out), 'w') as f:
    time_ds = f.create_dataset('time', shape=(args.nt,))

    t = 0.0
    for i in tqdm(range(args.nt), ncols=64):
        ode.advance(c, p, J, dt, args.ns, args.nr)

        ctime = etree.SubElement(coo, 'Grid')
        etree.SubElement(ctime, 'Time').attrib['Value'] = '{}'.format(t)
        ctime.attrib['Name'] = 'coo-step-{}'.format(i)
        ctime.attrib['GridType'] = 'Collection'
        ctime.attrib['CollectionType'] = 'Spatial'

        dtime = etree.SubElement(hist, 'Grid')
        etree.SubElement(dtime, 'Time').attrib['Value'] = '{}'.format(t)
        dtime.attrib['Name'] = 'nu-step-{}'.format(i)
        dtime.attrib['GridType'] = 'Collection'
        dtime.attrib['CollectionType'] = 'Spatial'

        t += dt * args.ns
        time_ds[i] = t

        for e in range(m):
            nu[e] += histogram2d(c[e,:,1], c[e,:,0], bins=(args.npy, args.npx), range=((0,H),(0,L)))[0]

            f['c-{}-{}'.format(e,i)] = vstack((zeros(n), c[e,:,0], c[e,:,1] + e * H * 1.1)).transpose().copy()
            f['nu-{}-{}'.format(e,i)] = nu[e] / (i + 1)

            # --- Points block
            grid = etree.SubElement(ctime, 'Grid')
            grid.attrib['Name']     = 'coo-{}-{}'.format(e,i)
            grid.attrib['GridType'] = 'Uniform'

            topo = etree.SubElement(grid, 'Topology')
            topo.attrib['TopologyType'] = 'Polyvertex'
            topo.attrib['NumberOfElements'] = '{}'.format(n)

            geom = etree.SubElement(grid, 'Geometry')
            geom.attrib['GeometryType'] = 'XYZ'

            data = etree.SubElement(geom, 'DataItem')
            data.attrib['Format'] = 'HDF'
            data.attrib['NumberType'] = 'Float'
            data.attrib['Precision'] = '8'
            data.attrib['Dimensions'] = '{} 3'.format(n)
            data.text = '{}.h5:/c-{}-{}'.format(args.out, e, i)

            # --- Points block
            grid = etree.SubElement(dtime, 'Grid')
            grid.attrib['Name']     = 'density-{}-{}'.format(e, i)
            grid.attrib['GridType'] = 'Uniform'

            topo = etree.SubElement(grid, 'Topology')
            topo.attrib['TopologyType'] = '2DCoRectMesh'
            topo.attrib['Dimensions'] = '{} {}'.format(args.npy+1, args.npx+1)

            geom = etree.SubElement(grid, 'Geometry')
            geom.attrib['GeometryType'] = 'Origin_DxDy'
            origin = etree.SubElement(geom, 'DataItem')
            origin.attrib['Format'] = 'XML'
            origin.attrib['NumberType'] = 'Float'
            origin.attrib['Dimensions'] = '2'
            origin.text = '{} 0.0'.format(e * H * 1.1)

            spacing = etree.SubElement(geom, 'DataItem')
            spacing.attrib['Format'] = 'XML'
            spacing.attrib['NumberType'] = 'Float'
            spacing.attrib['Dimensions'] = '2'
            spacing.text = '{period} {period}'.format(period=period)

            attr = etree.SubElement(grid, 'Attribute')
            attr.attrib['Name'] = 'density'
            attr.attrib['AttributeType'] = 'Scalar'
            attr.attrib['Center'] = 'Cell'

            data = etree.SubElement(attr, 'DataItem')
            data.attrib['Format'] = 'HDF'
            data.attrib['NumberType'] = 'Float'
            data.attrib['Precision'] = '8'
            data.attrib['Dimensions'] = '{} {}'.format(args.npy, args.npx)
            data.text = '{}.h5:/nu-{}-{}'.format(args.out, e, i)

with open('{}.xmf'.format(args.out), 'wb') as f:
    f.write(etree.tostring(xmf,
        pretty_print=True, xml_declaration=True, encoding='utf-8',
        doctype='<!DOCTYPE Xdmf SYSTEM "Sdmf.dtd" []>'))
