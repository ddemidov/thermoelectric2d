#ifndef TE_SYSTEM_HPP
#define TE_SYSTEM_HPP

#include <vexcl/vexcl.hpp>
#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/external/vexcl/vexcl.hpp>

namespace odeint = boost::numeric::odeint;

//---------------------------------------------------------------------------
typedef vex::vector<cl_double2> state_type;
typedef odeint::velocity_verlet<state_type> Stepper;

//---------------------------------------------------------------------------
VEX_FUNCTION(double, get_x, (cl_double2, p),
    return p.x;
);

VEX_FUNCTION(double, get_y, (cl_double2, p),
    return p.y;
);

VEX_FUNCTION(cl_double2, make_vec, (double, x)(double, y),
    double2 p = {x, y};
    return p;
);

VEX_FUNCTION(int, coo_to_cell, (cl_double2, coo)(double, C)(int, ncx)(int, ncy),
    int i = min(max(0, (int)(coo.x / C)), ncx-1);
    int j = min(max(0, (int)(coo.y / C)), ncy-1);
    return j * ncx + i;
);

VEX_FUNCTION(int, lo_bound, (int*, cell_id)(int, n)(int, nc)(int, id),
    int exp = id / nc; id = id % nc;
    int beg = exp * n;
    int end = beg + n;
    while(end > beg) {
        int mid = beg + (end - beg) / 2;
        if (cell_id[mid] < id)
            beg = mid + 1;
        else
            end = mid;
    }
    return beg;
);

VEX_FUNCTION(int, hi_bound, (int*, cell_id)(int, n)(int, nc)(int, id),
    int exp = id / nc; id = id % nc;
    int beg = exp * n;
    int end = beg + n;
    while(end > beg) {
        int mid = beg + (end - beg) / 2;
        if (cell_id[mid] <= id)
            beg = mid + 1;
        else
            end = mid;
    }
    return beg;
);

// #define PERIODIC_HORIZONTAL

//---------------------------------------------------------------------------
struct te_system {
    const int    n;    // Number of particles (electrons)
    const int    m;    // Number of experiments (entries in Edc, dT, K)
    const bool   periodic;
    const double screening; // Screening radius
    const double L;    // Domain length
    const double H;    // Domain height
    const double C;    // Interaction cut-off radius (L and H should be multiples of C)
    const double I;    // Coefficient in front of interaction potential (0.5 normally, may be set to 0 to get rid of interactions)
    const double eta;
    const double T0;   // Mean temperature

    const int ncx, ncy, nc;

    vex::vector<double>     Edc;
    vex::vector<double>     dT;
    vex::vector<cl_double2> K;

    vex::vector<int> cell_id, t_cell_id;
    vex::vector<int> order, t_order;
    vex::vector<int> cell_beg, cell_end;
    vex::vector<cl_double2> t;
    vex::vector<double> hf;

    vex::RandomNormal<double> rnd;
    vex::Reductor<int> count;
    vex::Reductor<double> sum;

    int cid;

    te_system(const vex::Context &ctx, int n, int m, bool periodic, double screening,
            double L, double H, double C, double I, double eta, double T0,
            const double *Edc, const double *dT, const cl_double2 *K)
        : n(n), m(m), periodic(periodic), screening(screening), L(L), H(H), C(C),
          I(I), eta(eta), T0(T0),
          ncx(ceil(L / C)), ncy(ceil(H / C)), nc(ncx * ncy),
          Edc(ctx, m, Edc), dT(ctx, m, dT), K(ctx, m, K),
          cell_id(ctx, n * m), t_cell_id(ctx, n), order(ctx, n * m), t_order(ctx, n),
          cell_beg(ctx, nc * m), cell_end(ctx, nc * m), t(ctx, n * m),
          hf(ctx, n * m), count(ctx), sum(ctx), cid(0)
    {
    }

    void operator()(const state_type &c, const state_type &p, state_type &dpdt, double t) {
        // Compute dpdt.
        // Particle interaction is always periodic,
        // so the only difference between 'periodic' and 'non-periodic'
        // boundary conditions is that in 'non-periodic' case particles are
        // bouncing off the left and right walls.
        VEX_FUNCTION(cl_double2, particle_interaction,
            (int, idx)(int, n)(int, ncx)(int, ncy)(cl_double2*, c)(double, C)(double, a_s)
            (double, L)(double, H)(int*, cell_beg)(int*, cell_end)(int*, order),

            int exp = idx / n;
            int nc  = ncx * ncy;

            cell_beg += exp * nc;
            cell_end += exp * nc;

            double2 coo = c[idx];

            int cj = min(max(0, (int)(coo.y / C)), ncy-1);
            int ci = min(max(0, (int)(coo.x / C)), ncx-1);

            double2 sum   = {0.0, 0.0};
            double2 shift = {0.0, 0.0};

            for(int j = -1; j <= 1; ++j) {
                int jj = cj + j;
                if (jj < 0) {
                    jj += ncy;
                    shift.y = -H;
                } else if (jj >= ncy) {
                    jj -= ncy;
                    shift.y = H;
                } else {
                    shift.y = 0.0;
                }

                for(int i = -1; i <= 1; ++i) {
                    int ii = ci + i;
                    if (ii < 0) {
                        ii += ncx;
                        shift.x = -L;
                    } else if (ii >= ncx) {
                        ii -= ncx;
                        shift.x = L;
                    } else {
                        shift.x = 0.0;
                    }

                    int cell_id = jj * ncx + ii;
                    for(int k = cell_beg[cell_id]; k < cell_end[cell_id]; ++k) {
                        int p = order[k];
                        if (p == idx) continue;

                        double2 d = coo - (c[p] + shift);
                        double r = sqrt(d.x * d.x + d.y * d.y + a_s * a_s);

                        if (r <= C) sum += d / (r * r * r);
                    }
                }
            }

            return sum;
        );

        auto idx = vex::tag<1>(vex::element_index(0, n * m));
        auto exp = vex::permutation(idx / n);

        if (I == 0.0) {
            dpdt = exp(K) * sin(c) + make_vec(exp(Edc), 0.0) - eta * p;
        } else {
            if (cid++ % 10 == 0) bind_to_cells(c, p);

            dpdt = exp(K) * sin(c) + make_vec(exp(Edc), 0.0) - eta * p
                + I * particle_interaction(
                    idx, n, ncx, ncy, raw_pointer(c), C, screening,
                    L, H, raw_pointer(cell_beg), raw_pointer(cell_end), raw_pointer(order));
        }
    }

    void sort_by_cell(const state_type &c, const state_type &p) {
        if (m == 1) {
            // Assign each particle to a cell, reset order:
            vex::tie(cell_id, order) = std::make_tuple(
                    coo_to_cell(c, C, ncx, ncy), vex::element_index(0, n));

            // Sort by experiment and cell numbers:
            vex::sort_by_key(cell_id, order);
        } else {
            for(int e = 0; e < m; ++e) {
                auto idx   = vex::tag<1>(vex::element_index(e * n, n));
                auto slice = vex::permutation(idx);

                vex::tie(t_cell_id, t_order) = std::make_tuple(
                        coo_to_cell(slice(c), C, ncx, ncy), idx);

                vex::sort_by_key(t_cell_id, t_order);

                vex::tie(slice(cell_id), slice(order)) = std::tie(t_cell_id, t_order);
            }
        }
    }

    void bind_to_cells(const state_type &c, const state_type &p) {
        sort_by_cell(c, p);

        // Find range of each cell in the order array:
        auto id = vex::tag<1>(vex::element_index(0, nc * m));
        vex::tie(cell_beg, cell_end) = std::tie(
                lo_bound(raw_pointer(cell_id), n, nc, id),
                hi_bound(raw_pointer(cell_id), n, nc, id));
    }

    void reorder(state_type &c, state_type &p) {
        sort_by_cell(c, p);

        // Apply the sorting to the particles to increase the cache locality:
        auto R = vex::permutation(order);

        t = R(c); c.swap(t);
        t = R(p); p.swap(t);

        bind_to_cells(c, p);
    }

    void fluctuate(const state_type &c, state_type &p, double dt, size_t seed) {
        VEX_FUNCTION(double, G, (cl_double2, c)(double, L)(double, T0)(double, dT)(double, eta),
            double T = T0 + (c.x / L - 0.5) * dT;
            return 2 * eta * T;
        );

        auto i = vex::tag<1>(vex::element_index(0, n * m));
        auto e = vex::permutation(i / n);
        auto g = vex::make_temp<2>(sqrt(dt * G(c, L, T0, e(dT), eta)));
        p += make_vec(g * rnd(i, seed), g * rnd(i + n * m, seed));
    }

    void fix_boundaries(state_type &c, state_type &p) {
        VEX_FUNCTION(void, fix, (double, L)(double, H)(cl_double2*, C)(cl_double2*, P)(int, i),
            double2 c = C[i];
            double2 p = P[i];

            if (c.x < 0) {
                c.x = -c.x;
                p.x = -p.x;
            }

            if (c.x > L) {
                c.x = 2 * L - c.x;
                p.x = -p.x;
            }

            if (c.y < 0) {
                c.y += H;
            }

            if (c.y > H) {
                c.y -= H;
            }

            C[i] = c;
            P[i] = p;
        );

        VEX_FUNCTION(void, fix_periodic, (double, L)(double, H)(cl_double2*, C)(int, i),
            double2 c = C[i];

            if (c.x < 0) {
                c.x += L;
            }

            if (c.x > L) {
                c.x -= L;
            }

            if (c.y < 0) {
                c.y += H;
            }

            if (c.y > H) {
                c.y -= H;
            }

            C[i] = c;
        );

        auto i = vex::tag<1>(vex::element_index(0, n * m));

        if (periodic) {
            vex::eval(fix_periodic(L, H, raw_pointer(c), i),
                    c.queue_list(), c.partition());
        } else {
            vex::eval(fix(L, H, raw_pointer(c), raw_pointer(p), i),
                    c.queue_list(), c.partition());
        }
    }

    void heat_flow(const state_type &c, const state_type &p, double *HF) {
        bind_to_cells(c, p);

        // Find heat flow.
        VEX_FUNCTION(double, flow,
            (int, idx)(int, n)(int, ncx)(int, ncy)(cl_double2*, c)(double, C)(double, a_s)
            (double, H)(int*, cell_beg)(int*, cell_end)(int*, order),

            int exp = idx / n;
            int nc  = ncx * ncy;

            cell_beg += exp * nc;
            cell_end += exp * nc;

            double2 coo = c[idx];

            int cj = min(max(0, (int)(coo.y / C)), ncy-1);
            int ci = min(max(0, (int)(coo.x / C)), ncx-1);

            double sum = 0.0;
            double2 shift = {0.0, 0.0};

            for(int j = -1; j <= 1; ++j) {
                int jj = cj + j;
                if (jj < 0) {
                    jj += ncy;
                    shift.y = -H;
                } else if (jj >= ncy) {
                    jj -= ncy;
                    shift.y = H;
                } else {
                    shift.y = 0.0;
                }

                for(int i = -1; i <= 1; ++i) {
                    int ii = ci + i;
                    if (ii < 0 || ii >= ncx) continue;

                    int cell_id = jj * ncx + ii;
                    for(int k = cell_beg[cell_id]; k < cell_end[cell_id]; ++k) {
                        int p = order[k];
                        if (p == idx) continue;

                        double2 d = coo - (c[p] + shift);
                        double r  = sqrt(d.x * d.x + d.y * d.y + a_s * a_s);

                        if (r <= C) sum += fabs(d.x) / (r * r * r);
                    }
                }
            }

            return -sum;
        );

        hf = flow(vex::element_index(0, n * m), n, ncx, ncy, raw_pointer(c), C,
                screening, H, raw_pointer(cell_beg),
                raw_pointer(cell_end), raw_pointer(order));

        for(int exp = 0; exp < m; ++exp) {
            auto slice = vex::permutation(vex::element_index(exp * n, n));

            // Count electrons in the middle third of the domain
            auto x = vex::make_temp<1>(get_x(slice(c)));
            int ne = count(x >= L/3 && x <= 2*L/3);

            auto J = vex::make_temp<2>(if_else(x >= L/3 && x <= 2*L/3,
                        slice(hf) * get_x(slice(p)), 0.0));
            HF[exp] = sum(J) / (2 * ne);
        }
    }
};

#endif
